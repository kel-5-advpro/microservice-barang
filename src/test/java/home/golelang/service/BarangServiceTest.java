package home.golelang.service;

import home.golelang.entity.Barang;
import home.golelang.repository.BarangRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BarangServiceTest {

    @InjectMocks
    private BarangService barangService;

    @Mock
    private BarangRepository barangRepository;

    @Test
    public void findBarangbyNamaBarangTest() {
        Mockito.when(barangRepository.findByNamaBarang("uhuy"))
                .thenReturn(new Barang());
        Barang barang = barangService.findBarangbyNamaBarang("uhuy");
        assertNotNull(barang);
    }

    @Test
    public void findAllBarangTest() {
        Mockito.when(barangRepository.findAll())
                .thenReturn(new ArrayList<Barang>(Arrays.asList(new Barang())));
        Iterable<Barang> barangs = barangService.findAllBarang();
        assertNotNull(barangs);
    }
}