package home.golelang.service;

import home.golelang.entity.ReviewBarang;
import home.golelang.repository.ReviewBarangRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReviewBarangServiceTest {

    @InjectMocks
    private ReviewBarangService reviewBarangService;

    @Mock
    private ReviewBarangRepository reviewBarangRepository;



    @Test
    public void findReviewBarangByNamaBarang() {
        Mockito.when(reviewBarangRepository.findReviewBarangByNamaBarang("uhuy")).thenReturn(new ArrayList<ReviewBarang>(Arrays.asList(new ReviewBarang())));
        Iterable<ReviewBarang> reviewBarangs = reviewBarangService.findReviewBarangByNamaBarang("uhuy");
        assertNotNull(reviewBarangs);
    }

    @Test
    public void findAllReviewBarang() {
        Mockito.when(reviewBarangRepository.findAll()).thenReturn(new ArrayList<ReviewBarang>(Arrays.asList(new ReviewBarang())));
        Iterable<ReviewBarang> reviewBarangs = reviewBarangService.findAllReviewBarang();
        assertNotNull(reviewBarangs);
    }

    @Test
    public void getReviewBarangById() {
        Mockito.when(reviewBarangRepository.findById(1)).thenReturn(new ReviewBarang());
        ReviewBarang reviewBarang = reviewBarangService.getReviewBarangById(1);
        assertNotNull(reviewBarang);
    }
}