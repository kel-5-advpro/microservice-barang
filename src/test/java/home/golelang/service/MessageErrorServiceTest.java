package home.golelang.service;

import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class MessageErrorServiceTest {

    MessageErrorService messageErrorService = new MessageErrorService();
    @Test(expected = NullPointerException.class)
    public void errorMessageService() {
        assertThat(messageErrorService.errorMessageService(null),
                instanceOf(ResponseEntity.class));
    }
}