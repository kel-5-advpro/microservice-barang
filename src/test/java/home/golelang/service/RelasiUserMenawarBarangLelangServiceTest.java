package home.golelang.service;

import home.golelang.entity.RelasiUserMenawarBarangLelang;
import home.golelang.repository.RelasiUserMemberikanReviewRepository;
import home.golelang.repository.RelasiUserMenawarBarangLelangRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RelasiUserMenawarBarangLelangServiceTest {

    @InjectMocks
    private RelasiUserMenawarBarangLelangService relasiUserMenawarBarangLelangService;

    @Mock
    private RelasiUserMenawarBarangLelangRepository relasiUserMenawarBarangLelangRepository;

    @Test
    public void getAllByNamaBarangFKTest() {
        Mockito.when(relasiUserMenawarBarangLelangRepository.getAllByNamaBarangFK("uhuy"))
                .thenReturn(new ArrayList<RelasiUserMenawarBarangLelang>(Arrays.asList(new RelasiUserMenawarBarangLelang())));
        ArrayList<RelasiUserMenawarBarangLelang> test = relasiUserMenawarBarangLelangService.getAllByNamaBarangFK("uhuy");
        assertNotNull(test);
    }

    @Test
    public void getAllByUserNameFKTest() {
        Mockito.when(relasiUserMenawarBarangLelangRepository.getAllByUserNameFK("uhuy"))
                .thenReturn(new ArrayList<RelasiUserMenawarBarangLelang>(Arrays.asList(new RelasiUserMenawarBarangLelang())));
        ArrayList<RelasiUserMenawarBarangLelang> test = relasiUserMenawarBarangLelangService.getAllByUserNameFK("uhuy");
        assertNotNull(test);
    }

    @Test(expected = NullPointerException.class)
    public void canChangeCurrentBidTest() {
        RelasiUserMenawarBarangLelang test = new RelasiUserMenawarBarangLelang();
        test.setUserBid(100);
        assertNull(relasiUserMenawarBarangLelangService.canChangeCurrentBid(test));
    }
}