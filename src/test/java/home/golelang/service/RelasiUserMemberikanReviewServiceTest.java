package home.golelang.service;

import home.golelang.entity.RelasiUserMemberikanReview;
import home.golelang.repository.RelasiUserMemberikanReviewRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RelasiUserMemberikanReviewServiceTest {

    @InjectMocks
    private RelasiUserMemberikanReviewService relasiUserMemberikanReviewService;

    @Mock
    private RelasiUserMemberikanReviewRepository relasiUserMemberikanReviewRepository;

    @Test
    public void getAllReviewByNameBarangFKTest() {
        Mockito.when(relasiUserMemberikanReviewRepository.getAllByNamaBarangFK("uhuy"))
                .thenReturn(new ArrayList<RelasiUserMemberikanReview>(Arrays.asList(new RelasiUserMemberikanReview())));
        ArrayList<RelasiUserMemberikanReview> test = relasiUserMemberikanReviewService
                                                        .getAllReviewByNameBarangFK("uhuy");
        assertNotNull(test);
    }

}