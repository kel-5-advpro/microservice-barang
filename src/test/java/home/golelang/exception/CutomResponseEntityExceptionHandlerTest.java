package home.golelang.exception;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class CutomResponseEntityExceptionHandlerTest {

    @Test
    public void handlerUserNameException() {
        CutomResponseEntityExceptionHandler cutomResponseEntityExceptionHandler =
                new CutomResponseEntityExceptionHandler();
        PrimaryKeyException response = new PrimaryKeyException("ccd");
        assertThat(cutomResponseEntityExceptionHandler.handlerUserNameException(response, null),
                instanceOf(ResponseEntity.class));
    }
}