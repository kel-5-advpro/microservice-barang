package home.golelang.exception;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrimaryKeyExceptionTest {

    PrimaryKeyException primaryKeyException;
    @Before
    public void setUp() throws Exception {
        primaryKeyException = new PrimaryKeyException("error");
    }

    @Test
    public void exceptionTest() {
        assertFalse(primaryKeyException.getMessage().isEmpty());
    }
}