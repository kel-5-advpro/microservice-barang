package home.golelang.exception;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrimaryKeyExceptionResponseTest {

    private PrimaryKeyExceptionResponse primaryKeyExceptionResponse;
    @Before
    public void setUp() throws Exception {
        primaryKeyExceptionResponse = new PrimaryKeyExceptionResponse("error");
    }

    @Test
    public void getPrimaryKey() {
        assertEquals(primaryKeyExceptionResponse.getPrimaryKey(),"error");
    }

    @Test
    public void setPrimaryKey() {
        primaryKeyExceptionResponse.setPrimaryKey("uhuy");
        assertEquals(primaryKeyExceptionResponse.getPrimaryKey(),"uhuy");
    }
}