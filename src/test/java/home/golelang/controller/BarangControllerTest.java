package home.golelang.controller;

import home.golelang.GoLelangApplication;
import home.golelang.entity.Barang;
import home.golelang.repository.BarangRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.json.JSONObject;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GoLelangApplication.class, properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BarangControllerTest {

    @Autowired
    private BarangRepository barangRepository;

    @LocalServerPort
    private int port;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + port;
        Barang barang = barangRepository.findByNamaBarang("Ciera");
    }



    @Test
    public void getBarangByNamaBarang() {
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang/b").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang/all").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang_lelang/all").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang_lelang/1").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang_lelang/all/false").then().assertThat()
                .statusCode(200);


        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/review/show/sadsad").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/review/all").then().assertThat()
                .statusCode(200);

        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang_lelang/user/sad").then().assertThat()
                .statusCode(200);
        RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/barang_lelang/barang/sad").then().assertThat()
                .statusCode(200);



    }

}