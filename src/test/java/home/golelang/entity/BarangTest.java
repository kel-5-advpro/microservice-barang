package home.golelang.entity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BarangTest {
    @Autowired
    private TestEntityManager entityManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Barang barang;

    private Barang barang2;

    @Before
    public void setUp() throws Exception {
        barang = new Barang();
        barang.setUrlFoto("url");
        barang.setNamaBarang("nama");
        barang.setStartingBid(200);

        barang2 = new Barang("nama2",200);
    }

    @Test
    public void getUrlFoto() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        assertThat(saveBarang.getUrlFoto()).isEqualTo("url");
    }

    @Test
    public void setUrlFoto() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        saveBarang.setUrlFoto("url2");
        assertThat(saveBarang.getUrlFoto()).isEqualTo("url2");
    }

    @Test
    public void getNamaBarang() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        assertThat(saveBarang.getNamaBarang()).isEqualTo("nama");
    }

    @Test
    public void setNamaBarang() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        saveBarang.setNamaBarang("nama2");
        assertThat(saveBarang.getNamaBarang()).isEqualTo("nama2");
    }

    @Test
    public void getStartingBid() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        assertThat(saveBarang.getStartingBid()).isEqualTo(200);
    }

    @Test
    public void setStartingBid() {
        Barang saveBarang = this.entityManager.persistAndFlush(barang);
        saveBarang.setStartingBid(300.323);
        assertThat(saveBarang.getStartingBid()).isEqualTo(300.323);
    }
}