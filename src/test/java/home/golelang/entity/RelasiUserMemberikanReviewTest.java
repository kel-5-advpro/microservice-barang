package home.golelang.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RelasiUserMemberikanReviewTest {
    RelasiUserMemberikanReview relasiUserMemberikanReview;

    @Before
    public void setUp() {
        relasiUserMemberikanReview = new RelasiUserMemberikanReview("user",
                "barang",new Long(2));
    }

    @Test
    public void getNamaBarangFK() {
        assertEquals(relasiUserMemberikanReview.getNamaBarangFK(), "barang");
    }

    @Test
    public void setNamaBarangFK() {
        relasiUserMemberikanReview.setNamaBarangFK("barang4");
        assertEquals(relasiUserMemberikanReview.getNamaBarangFK(), "barang4");
    }

    @Test
    public void getId() {
        assertEquals(relasiUserMemberikanReview.getId(), null);
    }

    @Test
    public void setId() {
        relasiUserMemberikanReview.setId(new Long(4));
        assertEquals(relasiUserMemberikanReview.getId(), new Long(4));
    }

    @Test
    public void getUserNameFK() {
        assertEquals(relasiUserMemberikanReview.getUserNameFK(), "user");
    }

    @Test
    public void setUserNameFK() {
        relasiUserMemberikanReview.setUserNameFK("barang4");
        assertEquals(relasiUserMemberikanReview.getUserNameFK(), "barang4");
    }

    @Test
    public void getIdReview() {
        assertEquals(relasiUserMemberikanReview.getIdReview(), new Long(2));
    }

    @Test
    public void setIdReview() {
        relasiUserMemberikanReview.setIdReview(new Long(99));
        assertEquals(relasiUserMemberikanReview.getIdReview(), new Long(99));
    }
}