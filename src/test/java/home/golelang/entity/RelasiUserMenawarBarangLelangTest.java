package home.golelang.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RelasiUserMenawarBarangLelangTest {

    private RelasiUserMenawarBarangLelang test;

    @Before
    public void setUp() throws Exception {
        long id = 10;
        test = new RelasiUserMenawarBarangLelang("nama", "barang", id, 99);
    }

    @Test
    public void onCreate() {
        test.onCreate();
    }

    @Test
    public void getId() {
        assertEquals(test.getId(), 0);
    }

    @Test
    public void setId() {
        test.setId(11);
        assertEquals(test.getId(), 11);
    }

    @Test
    public void getNamaBarangFK() {
        assertEquals(test.getNamaBarangFK(), "barang");
    }

    @Test
    public void setNamaBarangFK() {
        test.setNamaBarangFK("uhuy");
        assertEquals(test.getNamaBarangFK(), "uhuy");
    }

    @Test
    public void getUserNameFK() {
        assertEquals(test.getUserNameFK(), "nama");
    }

    @Test
    public void setUserNameFK() {
        test.setUserNameFK("bam");
        assertEquals(test.getUserNameFK(), "bam");
    }

    @Test
    public void getIdBarangLelang() {
        assertEquals(test.getIdBarangLelang(), 10);
    }

    @Test
    public void setIdBarangLelang() {
        test.setIdBarangLelang(11);
        assertEquals(test.getIdBarangLelang(), 11);
    }

    @Test
    public void getUserBid() {
        assertEquals(test.getUserBid(), 99, 0.0);
    }

    @Test
    public void setUserBid() {
        test.setUserBid(100);
        assertEquals(test.getUserBid(), 100, 0.0);
    }
}