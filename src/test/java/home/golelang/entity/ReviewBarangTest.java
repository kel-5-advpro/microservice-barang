package home.golelang.entity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ReviewBarangTest {

    @Autowired
    private TestEntityManager entityManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ReviewBarang reviewBarang;

    @Before
    public void setUp() throws Exception {
        reviewBarang = new ReviewBarang();
        reviewBarang.setId(1);
        reviewBarang.setNamaBarang("test");
        reviewBarang.setPesan("test123");
        reviewBarang.setRating(5.0);
    }

    @Test
    public void getId() {
        assertNotNull(reviewBarang.getId());
    }

    @Test
    public void setId() {
        reviewBarang.setId(2);
        assertEquals(reviewBarang.getId(),2);
    }

    @Test
    public void getNamaBarang() {
        assertNotNull(reviewBarang.getNamaBarang());
    }

    @Test
    public void setNamaBarang() {
        reviewBarang.setNamaBarang("uhuy");
        assertEquals(reviewBarang.getNamaBarang(),"uhuy");
    }

    @Test
    public void getPesan() {
        assertNotNull(reviewBarang.getPesan());
    }

    @Test
    public void setPesan() {
        reviewBarang.setPesan("uhuy123");
        assertEquals(reviewBarang.getPesan(),"uhuy123");
    }

    @Test
    public void getRating() {
        assertNotNull(reviewBarang.getRating());
    }

    @Test
    public void setRating() {
        reviewBarang.setRating(3.0);
        assertNotNull(reviewBarang.getRating());
    }
}