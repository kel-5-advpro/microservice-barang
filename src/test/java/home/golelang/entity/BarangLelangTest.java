package home.golelang.entity;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BarangLelangTest {

    private BarangLelang barangLelang;

    @Before
    public void setUp() {
        barangLelang = new BarangLelang();
        barangLelang.setUrlFoto("url");
        barangLelang.setCurrentBid(20);
        barangLelang.setHighestBidder("high");
        barangLelang.setNamaBarang("barang");
        barangLelang.setIdBarangLelang(1);
        barangLelang.setFinished(true);
        barangLelang.setBarangLelangStartDate(null);
        barangLelang.setBarangLelangEndDate(null);
    }

    @Test
    public void getUrlFoto() {

        assertThat(barangLelang.getUrlFoto()).isEqualTo("url");
    }

    @Test
    public void setUrlFoto() {

        barangLelang.setUrlFoto("url2");
        assertThat(barangLelang.getUrlFoto()).isEqualTo("url2");
    }

    @Test
    public void getBarangLelangStartDate() {

        assertThat(barangLelang.getBarangLelangStartDate()).isEqualTo(null);
    }

    @Test
    public void setBarangLelangStartDate() {

        barangLelang.setBarangLelangStartDate(null);
        assertThat(barangLelang.getBarangLelangStartDate()).isEqualTo(null);
    }

    @Test
    public void getBarangLelangEndDate() {

        assertThat(barangLelang.getBarangLelangEndDate()).isEqualTo(null);
    }

    @Test
    public void setBarangLelangEndDate() {

        barangLelang.setBarangLelangStartDate(null);
        assertThat(barangLelang.getBarangLelangEndDate()).isEqualTo(null);
    }

    @Test
    public void getIdBarangLelang() {

        assertThat(barangLelang.getIdBarangLelang()).isEqualTo(1);
    }

    @Test
    public void setIdBarangLelang() {

        barangLelang.setIdBarangLelang(2);
        assertThat(barangLelang.getIdBarangLelang()).isEqualTo(2);
    }

    @Test
    public void getNamaBarang() {

        assertThat(barangLelang.getNamaBarang()).isEqualTo("barang");
    }

    @Test
    public void setNamaBarang() {

        barangLelang.setNamaBarang("barang2");
        assertThat(barangLelang.getNamaBarang()).isEqualTo("barang2");
    }

    @Test
    public void getHighestBidder() {

        assertThat(barangLelang.getHighestBidder()).isEqualTo("high");
    }

    @Test
    public void setHighestBidder() {

        barangLelang.setHighestBidder("bim");
        assertThat(barangLelang.getHighestBidder()).isEqualTo("bim");
    }

    @Test
    public void getCurrentBid() {

        assertThat(barangLelang.getCurrentBid()).isEqualTo(20);
    }

    @Test
    public void setCurrentBid() {

        barangLelang.setCurrentBid(200);
        assertThat(barangLelang.getCurrentBid()).isEqualTo(200);
    }

    @Test
    public void isFinished() {

        assertThat(barangLelang.isFinished()).isEqualTo(true);
    }

    @Test
    public void setFinished() {

        barangLelang.setFinished(false);
        assertThat(barangLelang.isFinished()).isEqualTo(false);
    }
}