package home.golelang.service;

import home.golelang.entity.ReviewBarang;
import home.golelang.repository.ReviewBarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ReviewBarangService {
    @Autowired
    private ReviewBarangRepository reviewBarangRepository;

    public ReviewBarang addReview(ReviewBarang reviewBarang) {
        return reviewBarangRepository.save(reviewBarang);
    }

    public ArrayList<ReviewBarang> findReviewBarangByNamaBarang(String namaBarang) {
        return reviewBarangRepository.findReviewBarangByNamaBarang(namaBarang);
    }

    public Iterable<ReviewBarang> findAllReviewBarang(){
        return reviewBarangRepository.findAll();
    }

    public ReviewBarang getReviewBarangById(long id) {
        return reviewBarangRepository.findById(id);
    }
}
