package home.golelang.service;

import home.golelang.entity.BarangLelang;
import home.golelang.entity.RelasiUserMenawarBarangLelang;
import home.golelang.repository.BarangLelangRepository;
import home.golelang.repository.RelasiUserMenawarBarangLelangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RelasiUserMenawarBarangLelangService {
    @Autowired
    private RelasiUserMenawarBarangLelangRepository relasiUserMenawarBarangLelangRepository;

    @Autowired
    private BarangLelangRepository barangLelangRepository;

    public RelasiUserMenawarBarangLelang addForeignKeyUserBarangLelang(
            RelasiUserMenawarBarangLelang relasiUserMenawarBarangLelang){
        return relasiUserMenawarBarangLelangRepository.save(relasiUserMenawarBarangLelang);
    }

    public ArrayList<RelasiUserMenawarBarangLelang> getAllByNamaBarangFK(String namaBarangFK){
        return relasiUserMenawarBarangLelangRepository.getAllByNamaBarangFK(namaBarangFK);
    }

    public ArrayList<RelasiUserMenawarBarangLelang> getAllByUserNameFK(String userNameFK) {
        return relasiUserMenawarBarangLelangRepository.getAllByUserNameFK(userNameFK);
    }


    public boolean canChangeCurrentBid (RelasiUserMenawarBarangLelang relasiUserMenawarBarangLelang) {
        BarangLelang barangLelang = barangLelangRepository
                .getByIdBarangLelang(relasiUserMenawarBarangLelang.getIdBarangLelang());
        return barangLelang.getCurrentBid() < relasiUserMenawarBarangLelang.getUserBid();
    }
}
