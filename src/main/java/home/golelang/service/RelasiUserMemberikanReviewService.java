package home.golelang.service;

import home.golelang.entity.RelasiUserMemberikanReview;
import home.golelang.repository.RelasiUserMemberikanReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RelasiUserMemberikanReviewService {
    @Autowired
    private RelasiUserMemberikanReviewRepository relasiUserMemberikanReviewRepository;

    public RelasiUserMemberikanReview addForeignKeyUserBarang
            (String userNameFK, String namaBarangFK, Long idReview) {
        return relasiUserMemberikanReviewRepository.save(
                new RelasiUserMemberikanReview(userNameFK, namaBarangFK, idReview));
    }

    public ArrayList<RelasiUserMemberikanReview> getAllReviewByNameBarangFK(String namaBarang) {
        return relasiUserMemberikanReviewRepository.getAllByNamaBarangFK(namaBarang);
    }

}
