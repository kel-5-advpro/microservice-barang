package home.golelang.service;

import home.golelang.entity.Barang;
import home.golelang.entity.BarangLelang;
import home.golelang.exception.PrimaryKeyException;
import home.golelang.repository.BarangLelangRepository;
import home.golelang.repository.BarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class BarangLelangService {
    @Autowired
    private BarangLelangRepository barangLelangRepository;

    @Autowired
    private BarangRepository barangRepository;

    public BarangLelang getByIdIdBarangLelang(long idBarangLelang) {
        return barangLelangRepository.getByIdBarangLelang(idBarangLelang);
    }

    public BarangLelang addBarangLelang(BarangLelang barangLelang){
        try {
            Barang strongBarangLelang = barangRepository.findByNamaBarang(barangLelang.getNamaBarang());
            barangLelang.setCurrentBid(strongBarangLelang.getStartingBid());
            barangLelang.setUrlFoto(strongBarangLelang.getUrlFoto());
            return barangLelangRepository.save(barangLelang);
        } catch (Exception e){
            throw new PrimaryKeyException("BarangLealang '" + barangLelang.getNamaBarang()+"' already exist");
        }
    }

    public void updateBidById(long id, double currentBid, String userName) {
        BarangLelang barangLelang = barangLelangRepository.getByIdBarangLelang(id);
        barangLelang.setCurrentBid(currentBid);
        barangLelang.setHighestBidder(userName);
    }

    public ArrayList<BarangLelang> getAllBarangLelang(){
        return barangLelangRepository.findAll();
    }

    public BarangLelang getBarangLelangById(long id) {
        return barangLelangRepository.getByIdBarangLelang(id);
    }

    public ArrayList<BarangLelang> getAllByFinished(boolean finished) {
        return barangLelangRepository.findAllByFinished(finished);
    }
}
