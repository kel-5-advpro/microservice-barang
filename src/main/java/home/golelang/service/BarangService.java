package home.golelang.service;

import home.golelang.entity.Barang;
import home.golelang.exception.PrimaryKeyException;
import home.golelang.repository.BarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BarangService {
    @Autowired
    private BarangRepository barangRepository;

    public Barang addBarang(Barang barang) {
        try{
            return barangRepository.save(barang);
        } catch (Exception e){
            throw new PrimaryKeyException("Barang '"+barang.getNamaBarang()+"' already exist");
        }
    }

    public Barang findBarangbyNamaBarang(String namaBarang){
        Barang barang = barangRepository.findByNamaBarang(namaBarang);

        return barang;
    }

    public Iterable<Barang> findAllBarang() {
        return barangRepository.findAll();
    }
}
