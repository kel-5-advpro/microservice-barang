package home.golelang.repository;

import home.golelang.entity.Barang;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarangRepository extends CrudRepository<Barang, Long> {

    Barang findByNamaBarang(String namaBarang);

    Iterable<Barang> findAll();
}
