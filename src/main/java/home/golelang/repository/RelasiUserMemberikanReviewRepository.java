package home.golelang.repository;

import home.golelang.entity.RelasiUserMemberikanReview;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RelasiUserMemberikanReviewRepository
        extends CrudRepository<RelasiUserMemberikanReview, Long> {

    ArrayList<RelasiUserMemberikanReview> findAll();
    ArrayList<RelasiUserMemberikanReview> getAllByNamaBarangFK(String namaBarang);
}
