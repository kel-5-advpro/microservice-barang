package home.golelang.repository;

import home.golelang.entity.RelasiUserMenawarBarangLelang;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RelasiUserMenawarBarangLelangRepository
        extends CrudRepository<RelasiUserMenawarBarangLelang, Long> {

    ArrayList<RelasiUserMenawarBarangLelang> findAll();
    ArrayList<RelasiUserMenawarBarangLelang> getAllByNamaBarangFK(String namaBarangFK);
    ArrayList<RelasiUserMenawarBarangLelang> getAllByUserNameFK(String userNameFK);
    RelasiUserMenawarBarangLelang getByIdAndAndUserNameFK(long idBarangLelang, String userName);

}
