package home.golelang.repository;

import home.golelang.entity.BarangLelang;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface BarangLelangRepository extends CrudRepository<BarangLelang, Long> {
    BarangLelang getByIdBarangLelang(long idBarangLelang);
    ArrayList<BarangLelang> findAll();
    ArrayList<BarangLelang> findAllByFinished(boolean finished);
}
