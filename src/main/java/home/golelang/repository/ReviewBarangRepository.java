package home.golelang.repository;

import home.golelang.entity.ReviewBarang;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ReviewBarangRepository extends CrudRepository<ReviewBarang, Long> {

    ArrayList<ReviewBarang> findReviewBarangByNamaBarang(String namaBarang);

    Iterable<ReviewBarang> findAll();

    ReviewBarang findById(long id);
}
