package home.golelang.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PrimaryKeyException extends RuntimeException {

    public PrimaryKeyException(String message){
        super(message);
    }
}
