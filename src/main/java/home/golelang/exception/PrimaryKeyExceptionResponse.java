package home.golelang.exception;

public class PrimaryKeyExceptionResponse {

    private String primaryKey;

    public PrimaryKeyExceptionResponse(String primaryKey){
        this.primaryKey = primaryKey;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }
}