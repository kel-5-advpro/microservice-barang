package home.golelang.controller;

import home.golelang.entity.Barang;
import home.golelang.service.MessageErrorService;
import home.golelang.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/barang")
public class BarangController {

    @Autowired
    private BarangService barangService;

    @Autowired
    private MessageErrorService messageErrorService;

    @PostMapping("")
    public ResponseEntity<?> addNewBarang(@Valid @RequestBody Barang barang, BindingResult result){

        ResponseEntity<?> errorMessage = messageErrorService.errorMessageService(result);
        if(errorMessage != null){
            return errorMessage;
        }
        barangService.addBarang(barang);
        return new ResponseEntity<Barang>(barang, HttpStatus.CREATED);
    }

    @GetMapping("/{namaBarang}")
    public ResponseEntity<?> getBarangByNamaBarang(@PathVariable String namaBarang) {
        Barang barang = barangService.findBarangbyNamaBarang(namaBarang);
        return new ResponseEntity<Barang>(barang, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<Barang> getAllUser(){
        return barangService.findAllBarang();
    }

}
