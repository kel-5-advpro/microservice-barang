package home.golelang.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import home.golelang.entity.RelasiUserMemberikanReview;
import home.golelang.entity.ReviewBarang;
import home.golelang.service.MessageErrorService;
import home.golelang.service.RelasiUserMemberikanReviewService;
import home.golelang.service.ReviewBarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/review")
public class RelasiUserMemberikanReviewController {
    @Autowired
    private RelasiUserMemberikanReviewService relasiUserMemberikanReviewService;

    @Autowired
    private ReviewBarangService reviewBarangService;

    @Autowired
    private MessageErrorService messageErrorService;

    @PostMapping("/{userName}/{namaBarang}")
    public ResponseEntity<?> addNewReview
            (@PathVariable String userName, @PathVariable String namaBarang,
             @Valid @RequestBody ReviewBarang reviewBarang, BindingResult bindingResult) {
        ResponseEntity<?> errorMessage = messageErrorService.errorMessageService(bindingResult);
        if(errorMessage != null){
            return errorMessage;
        }
        reviewBarangService.addReview(reviewBarang);
        relasiUserMemberikanReviewService.addForeignKeyUserBarang(userName, namaBarang, reviewBarang.getId());
        return new ResponseEntity<ReviewBarang>(reviewBarang, HttpStatus.CREATED);
    }

    @GetMapping("/show/{namaBarang}")
    @Procedure("application/json")
    public ArrayList getAllReviewWithUserName(@PathVariable String namaBarang) {
        ArrayList<RelasiUserMemberikanReview> relasiUserMemberikanReviews = relasiUserMemberikanReviewService
                .getAllReviewByNameBarangFK(namaBarang);

        ArrayList jsonResponseArrayList = new ArrayList();
        for(RelasiUserMemberikanReview relasiUserMemberikanReview : relasiUserMemberikanReviews) {
            jsonResponseArrayList.add(
                    stringUserNameAndReviewToJson(relasiUserMemberikanReview.getUserNameFK(),
                    reviewBarangService.getReviewBarangById(relasiUserMemberikanReview.getId()).getPesan(),
                    relasiUserMemberikanReview.getNamaBarangFK(),
                    reviewBarangService.getReviewBarangById(relasiUserMemberikanReview.getId()).getRating())
            );
        }

        return jsonResponseArrayList;
    }

    @GetMapping("/all")
    public Iterable<ReviewBarang> getAllReview() {
        return reviewBarangService.findAllReviewBarang();
    }

    public JsonNode stringUserNameAndReviewToJson(String userName, String review, String namaBarang, double rating) {
        try {
            return new ObjectMapper()
                    .readTree("{\"userName\" : \"" + userName + "\","
                            + "\"pesan\" : \""+ review + "\","
                            + "\"namaBarang\" : \""+ namaBarang + "\","
                            + "\"rating\" : "+ rating +"}");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
