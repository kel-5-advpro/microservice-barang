package home.golelang.controller;

import home.golelang.entity.BarangLelang;
import home.golelang.entity.RelasiUserMenawarBarangLelang;
import home.golelang.service.BarangLelangService;
import home.golelang.service.MessageErrorService;
import home.golelang.service.RelasiUserMenawarBarangLelangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController()
@RequestMapping("/api/barang_lelang")
public class RelasiUserMenawarBarangLelangController {
    @Autowired
    private RelasiUserMenawarBarangLelangService relasiUserMenawarBarangLelangService;

    @Autowired
    private BarangLelangService barangLelangService;

    @Autowired
    private MessageErrorService messageErrorService;

    @PostMapping("/user_to_barang")
    public ResponseEntity<?> addUserToBarangLelang
            (@RequestBody RelasiUserMenawarBarangLelang relasiUserMenawarBarangLelang){

        if (!relasiUserMenawarBarangLelangService.canChangeCurrentBid(relasiUserMenawarBarangLelang)) {
            System.out.println("error");
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }

        barangLelangService.updateBidById(
                relasiUserMenawarBarangLelang.getIdBarangLelang(),
                relasiUserMenawarBarangLelang.getUserBid(),
                relasiUserMenawarBarangLelang.getUserNameFK());

        relasiUserMenawarBarangLelangService.addForeignKeyUserBarangLelang(
                relasiUserMenawarBarangLelang);

        return new ResponseEntity<RelasiUserMenawarBarangLelang>(
          relasiUserMenawarBarangLelang,HttpStatus.CREATED);
    }

    @GetMapping("/user/{userName}")
    public ArrayList<RelasiUserMenawarBarangLelang> getBarangLelangByUserName(@PathVariable String userName){
        return relasiUserMenawarBarangLelangService.getAllByUserNameFK(userName);
    }

    @GetMapping("/barang/{namaBarang}")
    public ArrayList<RelasiUserMenawarBarangLelang> getBarangLelangByNamaBarang(@PathVariable String namaBarang){
        return relasiUserMenawarBarangLelangService.getAllByNamaBarangFK(namaBarang);
    }
}
