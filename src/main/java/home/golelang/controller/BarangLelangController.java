package home.golelang.controller;

import home.golelang.entity.BarangLelang;
import home.golelang.service.BarangLelangService;
import home.golelang.service.MessageErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController()
@RequestMapping("/api/barang_lelang")
public class BarangLelangController {
    @Autowired
    private BarangLelangService barangLelangService;

    @Autowired
    private MessageErrorService messageErrorService;

    @PostMapping("")
    public ResponseEntity<?> addNewBarangLelang
            (@Valid @RequestBody BarangLelang barangLelang, BindingResult bindingResult){
        ResponseEntity<?> errorMessage = messageErrorService.errorMessageService(bindingResult);
        if(errorMessage != null){
            return errorMessage;
        }
        barangLelangService.addBarangLelang(barangLelang);
        return new ResponseEntity<BarangLelang>(barangLelang, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ArrayList<BarangLelang> getAllBarangLelang() {
        return barangLelangService.getAllBarangLelang();
    }

    @GetMapping("/{id}")
    public BarangLelang getBarangLelangById(@PathVariable long id) {
        return barangLelangService.getBarangLelangById(id);
    }

    @GetMapping("/all/{finished}")
    public ArrayList<BarangLelang> getBarangLelangByStatusFinished(@PathVariable boolean finished) {
        return barangLelangService.getAllByFinished(finished);
    }
}
