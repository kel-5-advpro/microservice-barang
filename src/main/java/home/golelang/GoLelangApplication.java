package home.golelang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoLelangApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoLelangApplication.class, args);
	}

}
