package home.golelang.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class BarangLelang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idBarangLelang;

    @NotBlank(message = "Nama barang is required")
    @Column(updatable = false, nullable = false)
    @Size(max = 50, message = "Max is 50 char")
    private String namaBarang;

    private String highestBidder;

    private double currentBid;

    private boolean finished;

    private String urlFoto;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date barangLelangStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date barangLelangEndDate;

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Date getBarangLelangStartDate() {
        return barangLelangStartDate;
    }

    public void setBarangLelangStartDate(Date barangLelangStartDate) {
        this.barangLelangStartDate = barangLelangStartDate;
    }

    public Date getBarangLelangEndDate() {
        return barangLelangEndDate;
    }

    public void setBarangLelangEndDate(Date barangLelangEndDate) {
        this.barangLelangEndDate = barangLelangEndDate;
    }

    public long getIdBarangLelang() {
        return idBarangLelang;
    }

    public void setIdBarangLelang(long idBarangLelang) {
        this.idBarangLelang = idBarangLelang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getHighestBidder() {
        return highestBidder;
    }

    public void setHighestBidder(String highestBidder) {
        this.highestBidder = highestBidder;
    }

    public double getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(double currentBid) {
        this.currentBid = currentBid;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
