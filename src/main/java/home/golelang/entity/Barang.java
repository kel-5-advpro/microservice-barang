package home.golelang.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Barang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Nama barang is required")
    @Column(updatable = false, unique = true, nullable = false)
    @Size(max = 50, message = "Max is 50 char")
    private String namaBarang;


    @NotNull(message = "Starting bid is required")
    private double startingBid;

    private String urlFoto;

    public Barang() {}

    public Barang(@NotBlank(message = "Nama barang is required") @Size(max = 50, message = "Max is 50 char") String namaBarang,
                  @NotNull(message = "Starting bid is required") double startingBid) {
        this.namaBarang = namaBarang;
        this.startingBid = startingBid;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String nama_barang) {
        this.namaBarang = nama_barang;
    }

    public double getStartingBid() {
        return startingBid;
    }

    public void setStartingBid(double startingBid) {
        this.startingBid = startingBid;
    }
}