package home.golelang.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class RelasiUserMenawarBarangLelang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String namaBarangFK;

    private String userNameFK;

    private long idBarangLelang;

    private double userBid;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date timestamp;

    public RelasiUserMenawarBarangLelang(String userNameFK, String namaBarangFK,
                                         long idBarangLelang, double userBid) {
        this.namaBarangFK = namaBarangFK;
        this.userNameFK = userNameFK;
        this.idBarangLelang = idBarangLelang;
        this.userBid = userBid;
    }

    public RelasiUserMenawarBarangLelang() {
    }

    @PrePersist
    public void onCreate() {
        this.timestamp = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamaBarangFK() {
        return namaBarangFK;
    }

    public void setNamaBarangFK(String namaBarangFK) {
        this.namaBarangFK = namaBarangFK;
    }

    public String getUserNameFK() {
        return userNameFK;
    }

    public void setUserNameFK(String userNameFK) {
        this.userNameFK = userNameFK;
    }

    public long getIdBarangLelang() {
        return idBarangLelang;
    }

    public void setIdBarangLelang(long idBarangLelang) {
        this.idBarangLelang = idBarangLelang;
    }

    public double getUserBid() {
        return userBid;
    }

    public void setUserBid(double userBid) {
        this.userBid = userBid;
    }
}
