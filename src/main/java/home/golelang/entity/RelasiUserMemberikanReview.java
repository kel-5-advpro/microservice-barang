package home.golelang.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class RelasiUserMemberikanReview {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userNameFK;

    private Long idReview;

    private String namaBarangFK;

    public RelasiUserMemberikanReview() {
    }

    public RelasiUserMemberikanReview(String userNameFK, String namaBarangFK, Long idReview) {
        this.userNameFK = userNameFK;
        this.namaBarangFK = namaBarangFK;
        this.idReview = idReview;
    }

    public String getNamaBarangFK() {
        return namaBarangFK;
    }

    public void setNamaBarangFK(String namaBarangFK) {
        this.namaBarangFK = namaBarangFK;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNameFK() {
        return userNameFK;
    }

    public void setUserNameFK(String userNameFK) {
        this.userNameFK = userNameFK;
    }

    public Long getIdReview() {
        return idReview;
    }

    public void setIdReview(Long idReview) {
        this.idReview = idReview;
    }
}
