package home.golelang.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class ReviewBarang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Nama barang is required")
    @Column(updatable = false, nullable = false)
    @Size(max = 50, message = "Max is 50 char")
    private String namaBarang;

    @NotBlank(message = "Pesan is required")
    @Column(updatable = true)
    @Size(max = 50, message = "Max is 50 char")
    private String pesan;

    @NotNull(message = "Rating is required")
    private double rating;

    public ReviewBarang() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
