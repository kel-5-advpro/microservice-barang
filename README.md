```bash
Kelompok    : 5
Kelas       : Advance Programming - B
Anggota     : Aloysius K. M. (1706040025)
              Aulia Rosyida (1706025346)
              Fakhira Devina (1706979221)
              Ferro Geraldi H. (1706028612)
              M. Feril Bagus P. (1706075054)

```
## Repositories
UI<br>
https://gitlab.com/kel-5-advpro/go-lelang
API<br>
https://gitlab.com/kel-5-advpro/microservice-barang

## Link Heroku
Go-Lelang<br>
https://go-lelang.herokuapp.com/
API<br>
http://microservice-barang.herokuapp.com/

## Deskripsi Projek <br>
http://bit.ly/descGoLelang

 
